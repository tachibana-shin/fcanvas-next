# @fcanvas/animate

Plugin that provides the ability to use animation for fcanvas

View source code at: https://github.com/tachibana-shin/fcanvas-next

### Install
```bash
pnpm add fcanvas @fcanvas/animate gsap
```

### Usage
```ts
import { Stage } from "fcanvas"
import { installAnimate } from "@fcanvas/animate"

installAnimate(Stage)
```

and now the `Animate` class is ready to import and the `.to()` method is up and running
